function loginForm() { //Kreira formu za login, tablica služi samo za poravnavanje labela i input-a.
    var form = '<h2 id="loginTitle">Prijava!</h2><br>';
    form += '<table><tr><td><label id="loginLabel" for="inputEmail">E-mail Adresa:</label></td><td><input type="email" id="inputEmail" aria-describedby="emailHelp" placeholder="Unesite Email Adresu!"></input></td></tr>';
    form += '<tr><td><label id="loginLabel" for="inputLozinka" id="loginlabel">Lozinka:</label></td><td><input type="password" id="inputLozinka" placeholder="Unesite Lozinku!"></input></td></tr></table>';
    form += '<br><br><button id="loginButton" type="submit">Prijavi se</button>'

    return form;
}

function buttonForm() { //Kreira buttone za print i insert nakon login-a.
    var form = '<button id="ispisiKorisnike">Print</button><button id="ispisiFormuZaInsert">Insert</button>';
    
    return form;
}


function insertForm() { //Kreira formu za insert.
    var form = '<table><tbody>';
    form += '<tr><th scope="col">Ime:</th><td><input type="text" id="ime"></td></tr>';
    form += '<tr><th scope="col">Prezime:</th><td><input type="text" id="prezime"></td></tr>';
    form += '<tr><th scope="col">Spol:</th>';
    form += '<td><select id="spol"><option value="1">Muško</option><option value="0">Žensko</option></select></td></tr>';
    form += '<tr><th scope="col">Email:</th><td><input type="email" id="email"></td></tr>';
    form += '<tr><th scope="col">Lozinka:</th><td><input type="password" id="lozinka"></td></tr>';
    form += '<tr><th scope="col">Datum rođenja:</th><td><input type="date" id="datum_rodenja"></td></tr>';
    form += '<tr><th><button type="submit" id="posaljiInsert">Insert</button></th><td><button type="cancel" id="cancel">Cancel</button></td></tr>';
    form += '</tbody></table>';

    return form;
}