var url = "https://dev.vub.zone/sandbox/router.php";

$(document).on('click', '#ispisiKorisnike', function (){ //Zove funkciju printKorisnici() kada se pritisne gumb ispisiKorisnike.
    printKorisnici();
});

$(document).on('click', '#ispisiFormuZaInsert', function (){ //Ispisuje formu za insert u sadržaj stranice kada se pritisne gumb ispisiFormuZaInsert.
    $("#sadrzaj").html(insertForm());
});

$(document).on('click', '#cancel', function (){ //Briše sadržaj stranice kada se pritisne gumb cancel.
    $("#sadrzaj").html('');
});

function printKorisnici() { //Spaja se na bazu i ispisuje sadržaj tablice korisnici u sadržaj stranice.
    //var tablica je varijabla u koju ćemo spremati generiranu tablicu.
    var tablica = '<table id="dataTable"><tbody><thead><tr><th scope="col">ID</th><th scope="col">IME</th><th scope="col">PREZIME</th><th scope="col">SPOL</th>';
    tablica += '<th scope="col">EMAIL</th><th scope="col">LOZINKA</th><th scope="col">DATUM RODENJA</th><th>EDIT</th>';
        
    $.ajax({ //Kreiranje API requesta prema tablici.
        type: 'POST', //Postavlja metodu request-a na POST.
        url: url,
        data: { "projekt": "p_kjugrogoz", "procedura": "p_printkorisnici"},
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var errcode = jsonBody.h_errcode;
            var message = jsonBody.h_message;
            var count   = jsonBody.count;


            if (message == null || message == "", errcode == null || errcode == 0) {
                $.each(jsonBody.data, function (key, value) { //Za svaki pronađeni red u tablici, generiramo sve ove podatke u varijabli tablica.
                    tablica += '<tr><td>' + value.ID + '</td>';
                    tablica += '<td>' + value.IME + '</td>';
                    tablica += '<td>' + value.PREZIME + '</td>';

                    //Ispisuje spol riječima umjesto brojkama.
                    if (value.SPOL == 1){
                        tablica += '<td>Muško</td>';
                    }
                    else if (value.SPOL == 0){
                        tablica += '<td>Žensko</td>';
                    }
                    else{
                        tablica += '<td>' + value.SPOL + '</td>';
                    }
                    
                    tablica += '<td>' + value.EMAIL + '</td>';
                    tablica += '<td>' + value.LOZINKA + '</td>';
                    tablica += '<td>' + value.DATUM_RODENJA + '</td>';

                    //Za svaki red dodaje gumb kojeg koristimo za edit-anje tog reda podataka.
                    tablica += '<td><button type="button" onclick="editKorisnici('+ value.ID + ')">Edit</button>';
                });
                tablica += '</tbody></table>';
                $("#sadrzaj").html(tablica); //Postavlja sadržaj stranice na tablicu.
            } else {
                if (errcode == 999) { //Ako nismo login-ani, zove login jer ne smijemo vidjeti tablicu ako nismo login-ani.
                    $("#sadrzaj").html(modalLogin());
                } else {
                    Swal.fire(message + '.' + errcode); //Prikazuje errore sa baze ako dođe do njih.
                }
            }
            $("#sadrzaj").html(tablica);
        },
        error: function (xhr, textStatus, error) { //Ajax errori.
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: true

    });
}


//Insert/update korisnika.
$(document).on('click', '#posaljiInsert', function () { //Šalje insert/update request na bazu prema unesenim podacima u formi.
    //Baza će napraviti insert ako ne pošaljemo vrijednost ID, a update ako pošaljemo neku vrijednost ID koja postoji u bazi.

    //Uzima podatke iz forme i sprema ih u varijable.
    var ID = $('#ID').val();
    var IME = $('#ime').val();
    var PREZIME = $('#prezime').val();
    var SPOL = $('#spol').val();
    var EMAIL = $('#email').val();
    var LOZINKA = $('#lozinka').val();
    var DATUM_RODENJA = $('#datum_rodenja').val();
    
    //Provjerava jesu li podaci koji MORAJU biti uneseni uneseni. (Podaci koji smiju biti null se ne provjeravaju)
    if (IME == null || IME == "") {
        Swal.fire('Molimo unesite Ime');
    } else if (PREZIME == null || PREZIME == "") {
        Swal.fire('Molimo unesite Prezime');
    } else if (EMAIL == null || EMAIL == "") {
        Swal.fire('Molimo unesite Email');    
    } else if (LOZINKA == null || LOZINKA == "") {
        Swal.fire('Molimo unesite lozinku');
    } else {
        $.ajax({
            type: 'POST',
            url: url,
            data: {"projekt": "p_kjugrogoz", "procedura": "p_insertkorisnici", "ID":ID, "ime": IME, "prezime": PREZIME, "spol": SPOL, "email": EMAIL, "lozinka": LOZINKA,"datum_rodenja": DATUM_RODENJA}, 
            success: function (data) {
                var jsonBody = JSON.parse(data);
                var errcode = jsonBody.h_errcode;
                var message = jsonBody.h_message;
                console.log(message+errcode);

                if ((message == null || message == "") && (errcode == null || errcode == 0)) {
                    Swal.fire('Uspješno se unijeli novog korisnika');
                } else {
                    Swal.fire(message);
                }
                printKorisnici(); //Nakon unosa/update-a ponovo ispisuje tablicu.
            },
            error: function (xhr, textStatus, error) {
                console.log(xhr.statusText);
                console.log(textStatus);
                console.log(error);
            },
            async: true
        });
    }
})


//Kada pritisnemo gumb edit zove se ova funkcija koja će generirati formu za edit tog korisnika.
//Funkcija se spaja na bazu i vraća podatke odabranog korisnika i stavlja ih u formu.
function editKorisnici(ID) {
    var tablica = '<table><tbody>';        
    $.ajax({
        type: 'POST',
        url: url,
        data: { "projekt": "p_kjugrogoz", "procedura": "p_editkorisnici", "ID": ID},
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var errcode = jsonBody.h_errcode;
            var message = jsonBody.h_message;
            
            if (message == null || message == "", errcode == null || errcode == 0) {
                $.each(jsonBody.data, function (key, value) { //Kada se pronađe odabrani korisnik, generiramo formu za njega.
                    //Input za ID će postojati, ali će biti skriven korisniku. To koristimo kako bi kasnije mogli uzeti tu vrijednost u funkciji insertKorisnici()
                    tablica += '<tr><input type="number" id="ID" value="' + value.ID + '" hidden></input><th scope="col">Ime:</th><td><input type="text" id="ime" value="' + value.IME + '"></td></tr>';
                    tablica += '<tr><th scope="col">Prezime:</th><td><input type="text" id="prezime" value="' + value.PREZIME + '"></td></tr>';
                    
                    //Postavlja vrijednost drop down selecta za spol na vrijednost koja je pronađena u bazi.
                    if(value.SPOL==1){
                        tablica += '<tr><th scope="col">Spol:</th><td><select id="spol"><option value="1" selected>Muško</option><option value="0">Žensko</option></select></td></tr>';
                    }
                    else if(value.SPOL==0){
                        tablica += '<tr><th scope="col">Spol:</th><td><select id="spol"><option value="1">Muško</option><option value="0" selected>Žensko</option></select></td></tr>';
                    }
                    else{
                        tablica += '<tr><th scope="col">Spol:</th><td><select id="spol"><option value="1">Muško</option><option value="0">Žensko</option></select></td></tr>';
                    }
                    
                    tablica += '<tr><th scope="col">Email:</th><td><input type="email" id="email" value="' + value.EMAIL + '"></td></tr>';
                    tablica += '<tr><th scope="col">Lozinka:</th><td><input type="password" id="lozinka" value="'+ value.LOZINKA + '"></td></tr>';
                    tablica += '<tr><th scope="col">Datum rođenja:</th><td><input type="date" id="datum_rodenja" value="' + value.DATUM_RODENJA + '"></td></tr>';
                    
                    //Generira gumbove za submit i cancel.
                    tablica += '<tr><th><button type="submit" id="posaljiInsert">Edit</button></th><td><button type="cancel" id="cancel">Cancel</button></td></tr>';
                    tablica += '</tbody></table>';
                });
                $("#sadrzaj").html(tablica);
            } else {
                if (errcode == 999) {
                    $("#sadrzaj").html(loginForm);
                } else {
                    Swal.fire(message);
                }
            }
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: true
    });
}