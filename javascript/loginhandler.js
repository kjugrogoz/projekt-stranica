$(document).ready(function() { //Pokreće modalni login kada se stranica load-a.
    modalLogin();
})

$.ajaxSetup({ //Postavlja postavke za ajax.
    xhrFields: {
        withCredentials: true
    }
});

var url = "https://dev.vub.zone/sandbox/router.php" //Globalna varijabla za url baze.

function modalLogin() {
    var modal = new tingle.modal({ //Kreira novi modal kojeg ćemo koristiti za login.
        footer: true,
        stickyFooter: false,
        closeMethods: ['overlay', 'button', 'escape'],
        closeLabel: "Close",
        cssClass: ['custom-class-1', 'custom-class-2'],

        onOpen: function() { //Kada se modal otvori
            $("#loginButton").click(function() {
                var email = $('#inputEmail').val();      //Sprema vrijednost emaila u varijablu.
                var lozinka = $('#inputLozinka').val();  //Sprema vrijednost lozinke u varijablu.
                if (email == null || email == "") {      //Ako nije unesena email adresa ispisuje da ju unesemo.
                    Swal.fire('Molimo unesite email adresu');
                } else if (lozinka == null || lozinka == "") { //Ako nije unesena lozinka ispisuje da ju unesemo.
                    Swal.fire('Molimo unesite lozinku');
                } else {
                    var output = login(); //Ako je sve uspješno uneseno, poziva funkciju login().
                }
                console.log("Rezultat pokušaja logina: " + output); //Ispisuje u konzolu je li login bio uspješan ili ne.
                if (output == 1){ //Ako je login uspješan, gasi modal i generira gumbe za print i insert.
                    $("#divButtons").html(buttonForm());
                    modal.close(); 
                }
            })
        },

        onClose: function() {
            console.log('Modal ugašen!'); //Ispisuje u konzolu da se modal ugasio.
        },

        beforeClose: function() {
            return true; //Zatvara modal.
        }
    });
    modal.setContent(loginForm()) //Postavlja sadržaj modala na formu koju generiramo u formhandler.js pomoću loginForm() funkcije.
    modal.open();
}


function login() {
    var output = 0;
    $.ajax({
        type: 'POST', //Postavlja metodu request-a na POST.
        url: url, //Koristi globalnu varijablu za url baze.
        data: {"projekt": "p_kjugrogoz", "procedura": "p_login", "email": $('#inputEmail').val(), "lozinka": $('#inputLozinka').val()}, //Spaja se na projekt p_kjugrogoz i poziva proceduru p_login sa danim vrijednostima.
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var errcod = jsonBody.h_errcod;
            var message = jsonBody.h_message;

            if (message == null || message == "", errcod == null || errcod == 0) { //Ako nema errora, postavi output na 1.
                output = 1;
            } else { //Ako je došlo do errora, ispiši error i postavi output na 0.
                Swal.fire(message + '.' + errcod);
                output = 0;
            }
        },
        error: function (xhr, textStatus, error) { //Error handling.
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: false
    });
    return output;
}